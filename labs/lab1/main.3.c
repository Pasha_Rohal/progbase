#include <stdio.h>
#include <progbase.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <math.h>
int main()
{
    int a = 0;
    int b = 0;
    int c = 0;
    Console_clear();
    puts("Enter a= ");
    a = getInt();
    puts("Enter b= ");
    b = getInt();
    puts("Enter c= ");
    c = getInt();

    printf("a = %i \n", a);
    printf("b = %i \n", b);
    printf("c = %i \n", c);

    if (a < 0 && b < 0 && c < 0)
    {
        int sum = a + b + c;
        int modmin1 = fmax(abs(a), abs(b));
        int modmin = fmax(abs(c), modmin1);
        int sum2 = a+b+c+modmin;
        puts("All negative");
        printf("sum = %i \n", sum);
        printf("sum2 = %i \n", sum2);
        printf("modmin = %i \n", modmin);
        if (abs(sum2) > 16 || modmin > 8)
        {
            puts("Result = True");
        }
        else
        {
            puts("Result = False");
        }
    }
    if (a >= 0 && b >= 0 && c >= 0)
    {
        int min1 = fmin(a, b);
        int min = fmin(c, min1);
        int max1 = fmax(a, b);
        int max = fmax(c, max1);
        puts("All positive");
        printf("min = %i \n", min);
        printf("max = %i \n", max);
        if ((max - min) > 32)
        {
            puts("Result = True");
        }
        else
        {
            puts("Result = False");
        }
    }
    if ((a < 0 && b >= 0 && c >= 0) || (b < 0 && c >= 0 && a >= 0) || (c < 0 && a >= 0 && b >= 0))
    {
        int min1 = fmin(a, b);
        int min = fmin(c, min1);
        puts("One negative");
        printf(" %i \n", min);
        if (min % 2 == 0)
        {
            puts("Result = True");
        }
        else
        {
            puts("Result = False");
        }
    }
    if ((a < 0 && b < 0 && c >= 0) || (b < 0 && c < 0 && a >= 0) || (c < 0 && a < 0 && b >= 0))
    {
        int kx = 0;
        int max1 = fmax(a, b);
        int max = fmax(c, max1);
        kx = a + b + c - max;
        puts("Two negative");
        if (max == a)
        {
            printf("%i \n", b);
            printf("%i \n", c);
        }
        if (max == b)
        {
            printf("%i \n", a);
            printf("%i \n", c);
        }
        if (max == c)
        {
            printf("%i \n", b);
            printf("%i \n", a);
        }

        if ((4 * kx) > -256)
        {
            puts("Result = True");
        }
        else
        {
            puts("Result = False");
        }
    }
    return 0;
}
