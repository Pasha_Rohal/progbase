#include <stdio.h>
#include <progbase.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <progbase/console.h>

struct Photo
{
    char author[32];
    int year;
};

struct PhotoData
{
    char name[300];
    char category[100];
    int likes;
    float persentLove;
    struct Photo photos;
};
enum
{
    MAX_PHOTOS = 10
};

struct list
{
    char name[300];
    int total;
    struct PhotoData **people;
};

//tests
bool peopleEquals(struct PhotoData *g1, struct PhotoData *g2);

bool PhotosEquals(struct list *g1, struct list *g2);

// menu
void clean(int i0, int i1, int j0, int j1);

void console(int size, int x);

int coice(int numb, int size);

//data
int photosToStr(struct PhotoData *pht, char *buf, int len);

int strToPhoto(const char *str, struct PhotoData *pht);

int PhotosArrayToStr(struct list *listofphoto, char *str, int len);

int strToPhotoArray(char str[], struct list *listofphoto);

struct PhotoData *newPhoto();

void freePhoto(struct PhotoData *p);

struct PhotoData *removePhotos(struct list *listofphoto, int index);

void enterPhoto(struct PhotoData *new);

bool addPhoto(struct list *listofphoto, struct PhotoData *pht);

bool newMining(struct PhotoData *pht, int mining);

struct PhotoData *photoAuthor(struct list *pht, int likes);

void printPhotosArray(struct list *pht);

void copyPhoto(struct PhotoData *g1, struct PhotoData *g2);

//file
long getFileSize(const char *fileName);

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength);

int fileExists(const char *fileName);

int main(int argc, char **argv)
{

    Console_clear();
    if (argc == 2 && !strcmp(argv[1], "-t"))
    {
        struct PhotoData p1 = {"name1", "selfi", 100, 50.5, .photos.author = "Sasha", .photos.year = 23};
        struct PhotoData p2 = {"name2", "sculpture", 0, 41.5, .photos.author = "Losha", .photos.year = 16};
        struct PhotoData p3 = {"name3", "wow", 1000, 95.5, .photos.author = "Vasya", .photos.year = 33};
        struct PhotoData p4 = {"name4", "loshad", 30, 84.9, .photos.author = "Volodya", .photos.year = 42};
        struct PhotoData p5 = {"name5", "godno", 100000, 11.9, .photos.author = "Kiril", .photos.year = 19};
        struct PhotoData p11 = {"", "", 0, 0.5, .photos.author = "", .photos.year = 0};
        struct PhotoData p22 = {"t", "sr", 0, 5, .photos.author = "L", .photos.year = 0};
        struct PhotoData p33 = {"", "pt", 0, 0.5, .photos.author = "", .photos.year = 0};
        struct PhotoData p44 = {"a", "", 0, 8.9, .photos.author = "", .photos.year = 0};
        struct PhotoData p55 = {"2", "", 0, 9, .photos.author = "", .photos.year = 0};
        char buf[1000];
        assert(photosToStr(&p1, buf, 1000) == 30);
        assert(photosToStr(&p2, buf, 1000) == 32);
        assert(photosToStr(&p3, buf, 1000) == 29);
        assert(photosToStr(&p4, buf, 1000) == 32);
        assert(photosToStr(&p5, buf, 1000) == 33);

        assert(strToPhoto("name1#selfi#100#50.50#Sasha#23", &p11) == 30);
        assert(strToPhoto("name2#sculpture#0#41.50#Losha#16", &p22) == 32);
        assert(strToPhoto("name3#wow#1000#95.50#Vasya#33", &p33) == 29);
        assert(strToPhoto("name4#loshad#30#84.90#Volodya#42", &p44) == 32);
        assert(strToPhoto("name5#godno#100000#11.90#Kiril#19", &p55) == 33);

        assert(peopleEquals(&p1, &p11));
        assert(peopleEquals(&p2, &p22));
        assert(peopleEquals(&p3, &p33));
        assert(peopleEquals(&p4, &p44));
        assert(peopleEquals(&p5, &p55));

        struct list l1 = {"name1", 10};
        struct list l2 = {"name2", 1};
        struct list l3 = {"name3", 0};
        struct list l4 = {"name4", 6};
        struct list l5 = {"name5", 2};
        struct PhotoData *people[10];
        people[0] = &p1;
        people[1] = &p2;
        people[2] = &p3;
        people[3] = &p4;
        people[4] = &p5;
        people[5] = &p11;
        people[6] = &p22;
        people[7] = &p33;
        people[8] = &p44;
        people[9] = &p55;
        l1.people = people;
        l2.people = people;
        l3.people = people;
        l4.people = people;
        l5.people = people;
        struct PhotoData p12;
        struct PhotoData p23;
        struct PhotoData p34;
        struct PhotoData p45;
        struct PhotoData p56;
        struct PhotoData p67;
        struct PhotoData p78;
        struct PhotoData p89;
        struct PhotoData p90;
        struct PhotoData p10;
        struct PhotoData *people2[10];
        people2[0] = &p12;
        people2[1] = &p23;
        people2[2] = &p34;
        people2[3] = &p45;
        people2[4] = &p56;
        people2[5] = &p67;
        people2[6] = &p78;
        people2[7] = &p89;
        people2[8] = &p90;
        people2[9] = &p10;
        struct list l11 = {"", 0};
        struct list l22 = {"", 0};
        struct list l33 = {"", 0};
        struct list l44 = {"", 0};
        struct list l55 = {"", 0};
        l11.people = people2;
        l22.people = people2;
        l33.people = people2;
        l44.people = people2;
        l55.people = people2;
        assert(!PhotosEquals(&l11, &l1));
        assert(!PhotosEquals(&l22, &l2));
        assert(!PhotosEquals(&l33, &l1));
        assert(!PhotosEquals(&l33, &l3));
        assert(!PhotosEquals(&l44, &l4));
        assert(!PhotosEquals(&l55, &l5));
        assert(removePhotos(&l1, 12) == NULL);
        assert(removePhotos(&l3, 2) == NULL);
        assert(removePhotos(&l2, -1) == NULL);
        removePhotos(&l1, 9);
        removePhotos(&l11, 9);
        removePhotos(&l5, 1);
        removePhotos(&l55, 1);
        assert(!PhotosEquals(&l11, &l1));
        assert(!PhotosEquals(&l55, &l5));
        people[9] = &p55;
        people[9] = &p10;
        addPhoto(&l1, &p1);
        addPhoto(&l11, &p1);
        assert(!PhotosEquals(&l1, &l11));
        assert(!addPhoto(&l1, &p2));
        assert(addPhoto(&l11, &p10));
        assert(addPhoto(&l2, &p23));
        assert(addPhoto(&l3, &p78));

        return 0;
    }
    const int consoleSize = 143;
    int comand = 0;
    do
    {
        console(consoleSize, 1);
        Console_setCursorPosition(2, consoleSize / 2 - 20);
        puts("       TAB Exit");
        Console_setCursorPosition(3, consoleSize / 2 - 20);
        puts("  Create new file.");
        Console_setCursorPosition(4, consoleSize / 2 - 20);
        puts("  Read file.");
        console(consoleSize, 5);
        int comand = coice(2, consoleSize);
        if (comand == -1)
        {
            Console_clear();
            int pl = 5;

            Console_setCursorAttribute(FG_DEFAULT);
            Console_setCursorPosition(pl, 1);
            return 0;
        }
        char buf[1250];
        int k = 0;
        struct list pht;
        struct PhotoData *people[10];
        struct PhotoData p0 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p1 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p2 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p3 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p4 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p5 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p6 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p7 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p8 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        struct PhotoData p9 = {"", "", 0, 0, .photos.author = "", .photos.year = 0};
        people[0] = &p0;
        people[1] = &p1;
        people[2] = &p2;
        people[3] = &p3;
        people[4] = &p4;
        people[5] = &p5;
        people[6] = &p6;
        people[7] = &p7;
        people[8] = &p8;
        people[9] = &p9;
        pht.people = people;
        struct list *p = &pht;
        if (comand == 1)
        {
            console(consoleSize, 1);
            Console_setCursorPosition(2, consoleSize / 2 - 20);
            printf("enter name of list : ");
            fgets(p->name, 32, stdin);
            p->name[strlen(p->name) - 1] = '\0';
            p->total = 0;
        }
        else if (comand == 2)
        {
            do
            {
                console(consoleSize, 1);
                Console_setCursorPosition(2, consoleSize / 2 - 20);
                printf("enter name file : ");
                char f[32];
                fgets(f, 31, stdin);
                f[strlen(f) - 1] = '\0';
                if (fileExists(f))
                {
                    long size = getFileSize(f);
                    char str[size + 1];
                    readFileToBuffer(f, str, size + 1);
                    str[size] = '\0';
                    k = 0;
                    for (int i = 0; str[i] != 0; i++)
                    {
                        buf[i] = str[i];
                    }
                    int n = strToPhotoArray(buf, p);
                    if (n <= 0 || n > 1250)
                    {
                        Console_setCursorPosition(3, consoleSize / 2 - 20);
                        Console_setCursorAttribute(FG_RED);
                        puts("not correct file");
                        Console_setCursorAttribute(FG_DEFAULT);
                        sleepMillis(1500);
                        Console_clear();
                        k = 1;
                    }
                }
                else
                {
                    Console_setCursorPosition(3, consoleSize / 2 - 20);
                    Console_setCursorAttribute(FG_BLUE);
                    printf("Not file %s\n", f);
                    Console_setCursorAttribute(FG_DEFAULT);
                    sleepMillis(1500);
                    Console_clear();
                    k = 1;
                }
            } while (k);
        }
        Console_clear();
        int comand2 = 0;
        do
        {
            Console_clear();
            console(consoleSize, 1);
            Console_setCursorPosition(2, consoleSize / 2 - 20);
            puts("Modify the list");
            Console_setCursorPosition(3, consoleSize / 2 - 20);
            puts("  Create new photo");
            Console_setCursorPosition(4, consoleSize / 2 - 20);
            puts("  Delete photo in position");
            Console_setCursorPosition(5, consoleSize / 2 - 20);
            puts("  Rewrite all data in photo");
            Console_setCursorPosition(6, consoleSize / 2 - 20);
            puts("  Rewrite some data in photo");
            Console_setCursorPosition(7, consoleSize / 2 - 20);
            puts("  search all photo more than X");
            Console_setCursorPosition(8, consoleSize / 2 - 20);
            puts("  Save list in FILE");
            Console_setCursorPosition(9, consoleSize / 2 - 20);
            puts("  Show list");
            console(consoleSize, 10);
            comand2 = coice(7, consoleSize);
            switch (comand2)
            {
            case 1:
            {
                console(consoleSize, 1);
                if (p->total < 10)
                {
                    struct PhotoData *new;
                    new = newPhoto();
                    addPhoto(p, new);
                    freePhoto(new);
                }
                else
                {
                    Console_setCursorAttribute(FG_RED);
                    Console_setCursorPosition(2, consoleSize / 2 - 20);
                    puts("The list is full");
                    Console_setCursorAttribute(FG_DEFAULT);
                    sleepMillis(1500);
                }
                break;
            }
            case 2:
            {
                if (p->total > 0)
                {
                    int k = 1;
                    do
                    {
                        console(consoleSize, 1);
                        puts("enter number positions");
                        int pos = getInt();
                        if (removePhotos(p, pos) == NULL)
                        {
                            Console_setCursorAttribute(FG_RED);
                            puts("Not correct");
                            Console_setCursorAttribute(FG_DEFAULT);
                            k = 1;
                        }
                        else
                            k = 0;
                    } while (k);
                }
                else
                {
                    console(consoleSize, 1);
                    puts("Empty list");
                    sleepMillis(1500);
                }
                break;
            }
            case 3:
            {
                if (p->total > 0)
                {
                    int k = 1;
                    do
                    {
                        console(consoleSize, 1);
                        puts("enter number positions");
                        int pos = getInt();
                        if (pos >= p->total || pos < 0)
                        {
                            Console_setCursorAttribute(FG_RED);
                            puts("Not correct");
                            Console_setCursorAttribute(FG_DEFAULT);
                            k = 1;
                        }
                        else
                        {
                            Console_clear();
                            console(consoleSize, 1);
                            enterPhoto(p->people[pos]);
                            k = 0;
                        }
                    } while (k);
                }
                else
                {
                    console(consoleSize, 1);
                    puts("Empty list");
                    sleepMillis(1500);
                }
                break;
            }
            case 4:
            {
                if (p->total > 0)
                {
                    int k = 1;
                    int pos = 0;
                    do
                    {
                        console(consoleSize, 1);
                        puts("enter number positions");
                        pos = getInt();
                        if (pos >= p->total || pos < 0)
                        {
                            Console_setCursorAttribute(FG_RED);
                            puts("Not correct");
                            Console_setCursorAttribute(FG_DEFAULT);
                            k = 1;
                        }
                        else
                        {
                            k = 0;
                        }
                    } while (k);
                    Console_clear();
                    console(consoleSize, 1);
                    Console_setCursorPosition(2, consoleSize / 2 - 20);
                    puts("What  change?");
                    Console_setCursorPosition(3, consoleSize / 2 - 20);
                    puts(" Name ");
                    Console_setCursorPosition(4, consoleSize / 2 - 20);
                    puts("  Category");
                    Console_setCursorPosition(5, consoleSize / 2 - 20);
                    puts("  likes");
                    Console_setCursorPosition(6, consoleSize / 2 - 20);
                    puts("  persent");
                    Console_setCursorPosition(7, consoleSize / 2 - 20);
                    puts("  author name");
                    Console_setCursorPosition(8, consoleSize / 2 - 20);
                    puts("  Year");
                    console(consoleSize, 9);
                    int mining = coice(6, consoleSize);
                    newMining(p->people[pos], mining);
                }
                else
                {
                    console(consoleSize, 1);
                    puts("Empty list");
                    sleepMillis(1500);
                }
                break;
            }
            case 5:
            {
                if (p->total > 0)
                {
                  console(consoleSize, 1);
                    puts("enter X");                  
                    int likes=0;
                    scanf("%i",&likes);
                    clean(2, 4, 1, 20);
                    Console_setCursorPosition(2, 1);
                    struct PhotoData *inX = photoAuthor(p, likes);
                    free(inX);
                }
                else
                {
                    console(consoleSize, 1);
                    puts("Empty list");
                    sleepMillis(1500);





                }
                getchar();
                break;
            }
            case 6:
            {
                console(consoleSize, 1);
                puts("Enter name file");
                char file[32];
                fgets(file, 31, stdin);
                file[strlen(file) - 1] = '\0';
                FILE *fp;
                fp = fopen(file, "w");
                PhotosArrayToStr(p, buf, 1250);
                fputs(buf, fp);
                fclose(fp);
                break;
            }
            case 7:
            {
                console(consoleSize, 1);
                printPhotosArray(p);
                console(consoleSize, p->total + 4);
                Console_getChar();
                break;
            }
            }
        } while (comand2 != -1);
    } while (comand != -1);
    return EXIT_SUCCESS;
}

void clean(int i0, int i1, int j0, int j1)
{
    Console_setCursorAttribute(BG_DEFAULT);
    for (int i = i0; i < i1; i++)
    {
        for (int j = j0; j < j1; j++)
        {
            Console_setCursorPosition(i, j);
            putchar(' ');
        }
    }
}

int coice(int numb, int size)
{
    Console_hideCursor();
    char n = 0;
    int comand = 0;
    Console_setCursorPosition(comand + 3, size / 2 - 20);
    putchar('>');
    while (n != 10)
    {
        n = Console_getChar();
        switch (n)
        {
        case 'w':
        {
            clean(3, numb + 3, size / 2 - 20, size / 2 - 19);
            clean(numb + 4, numb + 5, size / 2 - 20, size / 2 + 20);
            if (!comand)
            {
                comand = numb - 1;
            }
            else
            {
                comand--;
            }
            Console_setCursorPosition(comand + 3, size / 2 - 20);
            putchar('>');
            break;
        }
        case 's':
        {
            clean(3, numb + 3, size / 2 - 20, size / 2 - 19);
            clean(numb + 4, numb + 5, size / 2 - 20, size / 2 + 20);
            if (comand == numb - 1)
            {
                comand = 0;
            }
            else
            {
                comand++;
            }
            Console_setCursorPosition(comand + 3, size / 2 - 20);
            putchar('>');
            break;
        }
        case 10:
        {
            Console_clear();
            Console_showCursor();
            return comand + 1;
        }
        case 9:
        {
            Console_clear();
            Console_showCursor();
            return -1;
        }
        default:
        {
            Console_setCursorPosition(numb + 4, size / 2 - 20);
            Console_setCursorAttribute(FG_RED);
            puts("error, ( w - up, s - down, TAB - esc)");
            Console_setCursorAttribute(FG_DEFAULT);
            break;
        }
        }
    }
    Console_showCursor();
    return comand + 1;
}

long getFileSize(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return -1;
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return 0;
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;
}
int fileExists(const char *fileName)
{
    FILE *f = fopen(fileName, "rb");
    if (!f)
        return 0;
    fclose(f);
    return 1;
}
int photosToStr(struct PhotoData *pht, char *buf, int len)
{
    int nwrite = snprintf(buf, len, "%s#%s#%i#%.2f#%s#%i", pht->name, pht->category, pht->likes, pht->persentLove, pht->photos.author, pht->photos.year);
    return nwrite;
}

void copyPhotos(struct PhotoData *g1, struct PhotoData *g2)
{
    strcpy(g1->name, g2->name);
    strcpy(g1->category, g2->category);
    strcpy(g1->photos.author, g2->photos.author);
    g1->likes = g2->likes;
    g1->persentLove = g2->persentLove;
    g1->photos.year = g2->photos.year;
}

void printPhotosArray(struct list *pht)
{
    Console_setCursorPosition(2, 1);
    puts(pht->name);
    Console_setCursorPosition(3, 1);
    printf("Total[%i]", pht->total);
    for (int i = 0; i < MAX_PHOTOS && i < pht->total; i++)
    {
        Console_setCursorPosition(4 + i, 1);
        printf("%i)  ", i + 1);
        if (pht == NULL)
        {
            puts("( )");
        }
        else
            printf("%s, category: \"%s\", likes: %i, persentLove: %.2f, created by %s, author years %i", pht->people[i]->name, pht->people[i]->category, pht->people[i]->likes, pht->people[i]->persentLove, pht->people[i]->photos.author, pht->people[i]->photos.year);
    }
}

void console(int size, int x)
{
    Console_setCursorPosition(x, 0);

    Console_setCursorAttribute(FG_BLUE);
    for (int i = 0; i < size; i++)
    {
        putchar(' ');
    }

    Console_setCursorAttribute(FG_DEFAULT);
    Console_setCursorPosition(x + 1, 0);
}

int PhotoToStr(struct PhotoData *pht, char *buf, int len)
{
    int nwrite = snprintf(buf, len, "%s#%s#%i#%.2f#%s#%i", pht->name, pht->category, pht->likes, pht->persentLove, pht->photos.author, pht->photos.year);
    return nwrite;
}

int strToPhoto(const char *str, struct PhotoData *pht)
{
    char a = 0;
    int nread = 0;
    sscanf(str, "%99[^#]%*c%31[^#]%*c%i%c%f%c%31[^#]%*c%i%n",pht->name,pht->category,&(pht->likes),&a,&(pht->persentLove),&a,pht->photos.author,&(pht->photos.year),&nread);
    return nread;
}

int PhotosArrayToStr(struct list *listofphoto, char *str, int len)
{
    int nwrite = snprintf(str, len, "%s|%i\n", listofphoto->name, listofphoto->total);
    int totalWrite = nwrite;
    for (int i = 0; i < listofphoto->total; i++)
    {
        struct PhotoData *pht = listofphoto->people[i];
        totalWrite += PhotoToStr(pht, str + totalWrite, len - totalWrite);
        totalWrite += snprintf(str + totalWrite, len - totalWrite, "\n");
    }
    return totalWrite;
}

int strToPhotoArray(char str[], struct list *listOfphoto)
{
    int nread = 0;
    sscanf(str, "%31[^|]%*c%i\n%n", listOfphoto->name, &(listOfphoto->total), &nread);
    int totalRead = nread;
    if (nread > 0 && nread < 1250)
    {
        for (int i = 0; i < listOfphoto->total && i < MAX_PHOTOS; i++)
        {
            struct PhotoData *art = listOfphoto->people[i];
            totalRead += strToPhoto(str + totalRead, art);
            sscanf(str + totalRead, "\n%n", &nread);
            totalRead += nread;
        }
    }
    return totalRead;
}

void enterPhoto(struct PhotoData *new)
{
    puts("enter name photo");
    fgets(new->name, 31, stdin);
    new->name[strlen(new->name) - 1] = '\0';
    clean(2, 4, 1, 100);
    Console_setCursorPosition(2, 1);
    puts("enter category");
    fgets(new->category, 31, stdin);
    new->category[strlen(new->category) - 1] = '\0';
    clean(2, 4, 1, 100);
    Console_setCursorPosition(2, 1);
    puts("enter likes");
    new->likes = getInt();
    clean(2, 4, 1, 100);
    Console_setCursorPosition(2, 1);
    puts("enter persentLove");
    new->persentLove = getFloat();
    clean(2, 4, 1, 100);
    Console_setCursorPosition(2, 1);
    puts("enter author");
    fgets(new->photos.author, 31, stdin);
    new->photos.author[strlen(new->photos.author) - 1] = '\0';
    clean(2, 4, 1, 100);
    Console_setCursorPosition(2, 1);
    puts("enter year author");
    new->photos.year = getInt();
}

struct PhotoData *newPhoto()
{
    struct PhotoData *new = malloc(sizeof(struct PhotoData));
    enterPhoto(new);
    return new;
}

bool newMining(struct PhotoData *pht, int mining)
{
    switch (mining)
    {
    case 1:
    {
        puts("enter name photo");
        fgets(pht->name, 31, stdin);
        pht->name[strlen(pht->name) - 1] = '\0';
        break;
    }
    case 2:
    {
        puts("enter category");
        fgets(pht->category, 31, stdin);
        pht->category[strlen(pht->category) - 1] = '\0';
        break;
    }
    case 3:
    {
        puts("enter likes");
        pht->likes = getInt();
        break;
    }
    case 4:
    {
        puts("enter persentLove");
        pht->persentLove = getFloat();
        break;
    }
    case 5:
    {
        puts("enter name author");
        fgets(pht->photos.author, 31, stdin);
        pht->photos.author[strlen(pht->photos.author) - 1] = '\0';
        break;
    }
    case 6:
    {
        puts("enter year author");
        pht->photos.year = getInt();
        break;
    }
    }
    return true;
}

struct PhotoData *removePhotos(struct list *listofphoto, int index)
{
    if (index < 0 || index >= listofphoto->total)
        return NULL;
    struct PhotoData *removed = listofphoto->people[index];
    if (index < MAX_PHOTOS - 1)
    {
        for (int i = index; i < listofphoto->total; i++)
        {
            listofphoto->people[i] = listofphoto->people[i + 1];
        }
    }
    else
    {
        listofphoto->people[index] = NULL;
    }
    listofphoto->total--;
    return removed;
}

bool addPhoto(struct list *listofphoto, struct PhotoData *pht)
{
    if (listofphoto->total == MAX_PHOTOS)
    {
        return false;
    }
    strcpy(listofphoto->people[listofphoto->total]->name, pht->name);
    strcpy(listofphoto->people[listofphoto->total]->category, pht->category);
    strcpy(listofphoto->people[listofphoto->total]->photos.author, pht->photos.author);
    listofphoto->people[listofphoto->total]->persentLove = pht->persentLove;
    listofphoto->people[listofphoto->total]->likes = pht->likes;
    listofphoto->people[listofphoto->total]->photos.year = pht->photos.year;
    listofphoto->total++;
    return true;
}

struct PhotoData *photoAuthor(struct list *pht, int likes)
{
    int n = 0;
    for (int i = 0; i < pht->total; i++)
    {
        if (pht->people[i]->likes < likes)
        {
            n++;
            if (n == 1)
            {
                printf("you entered %i\n", likes);
            }
            printf("%i) %s, category: \"%s\", likes: %i, persentLove: %.2f, years  %i\n", n, pht->people[i]->name, pht->people[i]->category, pht->people[i]->likes, pht->people[i]->persentLove, pht->people[i]->photos.year);
        }
    }
    struct PhotoData * p = malloc(sizeof(struct PhotoData)* (n));
    if (n == 0)
    {
        Console_setCursorAttribute(FG_RED);
        puts("no one photo");
    } else {
        int k = 0;
        for (int i = 0; i < pht->total; i++)
        {
            if (pht->people[i]->likes < likes) 
            {
                copyPhotos(p, pht->people[i]);
                k++;
            }
        }
    }
    Console_setCursorAttribute(FG_DEFAULT);
    Console_getChar();
    return p;
}

void freePhoto(struct PhotoData *p)
{
    free(p);
}

bool peopleEquals(struct PhotoData *g1, struct PhotoData *g2)
{
    if (!strcmp(g1->name, g2->name) && !strcmp(g1->photos.author, g2->photos.author) && !strcmp(g1->category, g2->category) && g1->likes == g2->likes && fabs(g1->persentLove - g2->persentLove) < 0.01 && g1->photos.year == g2->photos.year)
    {
        return true;
    }
    return false;
}

bool PhotosEquals(struct list *g1, struct list *g2)
{
    if (!strcmp(g1->name, g2->name) && g1->total == g2->total)
    {
        for (int i = 0; i < g1->total; i++)
        {
            if (!peopleEquals(g1->people[i], g2->people[i]))
            {
                return false;
            }
        }
        return true;
    }
    else
        return false;
    return false;
}
