#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include <progbase.h>
#include <progbase.h>
#include <progbase/console.h>

struct Paragraph
{
    int index;
    char string[300];
};

struct ParagraphArray
{
    struct Paragraph *ParArr;
    int size;
};

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength);
int fileExists(const char *fileName);
int firstPar(struct Paragraph Paragraph1, struct Paragraph Paragraph2);
int secondPar(struct ParagraphArray arr1, struct ParagraphArray arr2);
int f1(const char text[]);
long getFileSize(const char *fileName);
void arrayFree(struct ParagraphArray *array);
void f2(const char text[], char buf[], int size);
void arrayFree(struct ParagraphArray *array);
struct Paragraph stringParagraph(const char string[]);
struct ParagraphArray *getAllParagraphsArrayNew(const char *str);

int main(int argc, char *argv[]) 
{
    char text3[]="Anyone who reads Old and Middle English literary texts will be familiar...";
    char text[] = "London is the capital of Great Britain, its political, economic and cultural centre. It’s one of the largest cities in the world. Its population is more than 9 million people. London is situated on the river Thames.\nIt was founded more than two thousand years ago.\n\nLondon is an ancient city. It appeared at the place where the Roman invaders decided to build a bridge over the Thames.\nThere are four parts in London: West End, East End, the City and Westminster.";
    char buf[500];
    char text2[] = "Ruslan Anatolievich the best prepodovatel in the world";
    struct Paragraph Paragraph1 = {0, "London is the capital of Great Britain, its political, economic and cultural centre. It’s one of the largest cities in the world. Its population is more than 9 million people. London is situated on the river Thames.\nIt was founded more than two thousand years ago."};
    struct Paragraph Paragraph2 = {268, "London is an ancient city. It appeared at the place where the Roman invaders decided to build a bridge over the Thames.\nThere are four parts in London: West End, East End, the City and Westminster."};
    struct Paragraph array[2] = {
        {0, "London is the capital of Great Britain, its political, economic and cultural centre. It’s one of the largest cities in the world. Its population is more than 9 million people. London is situated on the river Thames.\nIt was founded more than two thousand years ago."},
        {268, "London is an ancient city. It appeared at the place where the Roman invaders decided to build a bridge over the Thames.\nThere are four parts in London: West End, East End, the City and Westminster."}
    };
    struct Paragraph array1[2] = {
        {0, "London is the capital of Great Britain, its political, economic and cultural centre. It’s one of the largest cities in the world. Its population is more than 9 million people. London is situated on the river Thames.\nIt was founded more than two thousand years ago."},
        {268, "London is an ancient city. It appeared at the place where the Roman invaders decided to build a bridge over the Thames.\nThere are four parts in London: West End, East End, the City and Westminster."}
    };
    struct ParagraphArray p1 = {&array[0], 2 * sizeof(struct Paragraph)};
    struct ParagraphArray p2 = {&array1[0], 2 * sizeof(struct Paragraph)};
    if (argc == 2) {
        if (!strcmp(argv[1], "task1")) {
            assert(f1(text3) == -1);           
            assert(f1(text2) == -1);
            assert(f1(text) == 269);
            f2(text, buf, 500);
        }else if (!strcmp(argv[1], "task2")) {
            f2(text, buf, 500);
            assert(firstPar(Paragraph1, Paragraph1) == 1);
            assert(firstPar(Paragraph1, Paragraph2) == 0);
            assert(firstPar(Paragraph2, Paragraph2) == 1);
            assert(!strcmp(buf, "London is the capital of Great Britain, its political, economic and cultural centre. It’s one of the largest cities in the world. Its population is more than 9 million people. London is situated on the river Thames.\nIt was founded more than two thousand years ago."));
            struct Paragraph ones = stringParagraph(text);
            assert(firstPar(array[0], ones));
            assert(secondPar(p1, p2));
            assert(firstPar(stringParagraph(text), array[0]));
        }else if (!strcmp(argv[1], "task3")) {
            
            assert(secondPar(p1, *getAllParagraphsArrayNew(text)));
            assert(secondPar(p1, *getAllParagraphsArrayNew(text2))==0);
            assert(secondPar(p1, *getAllParagraphsArrayNew(text3))==0);
            return EXIT_SUCCESS;
        } else {
            return EXIT_FAILURE;
        }
    }

    return 0;
}

int f1(const char text[])
{
    int i = 0;
    for (i = 0; text[i] != 0; i++)
    {
        if (text[i] == '\n' && text[i + 1] == '\n' && text[i + 3] != 0)
        {
            return i + 3;
        }
    }
    return -1;
}
void f2(const char text[], char buf[], int size)
{
    int len = f1(text);
    if (len - 3 >= 0 && len < size)
    {
        strncpy(buf, text, len - 3);
        buf[len - 3] = 0;
    }
}
int firstPar(struct Paragraph Paragraph1, struct Paragraph Paragraph2)
{
    if (Paragraph1.index == Paragraph2.index)
    {
        if (!strcmp(Paragraph1.string, Paragraph2.string))
        {
            return 1;
        }
    }
    return 0;
}

int secondPar(struct ParagraphArray arr1, struct ParagraphArray arr2)
{
    if (arr1.size == arr2.size)
    {
        for (int i = 0; i + 1 <= arr1.size / sizeof(struct Paragraph); i++)
        {
            if (firstPar(arr1.ParArr[i], arr2.ParArr[i]))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
    return 0;
}

struct Paragraph stringParagraph(const char string[])
{
    struct Paragraph new;
    new.index = 0;
    f2(string, new.string, 300);
    return new;
}

void arrayFree(struct ParagraphArray *array)
{
    free(array->ParArr);
    free(array);
}

struct ParagraphArray *getAllParagraphsArrayNew(const char *str)
{
    int newsize = 10;
    struct ParagraphArray *ParArr = malloc(sizeof(struct ParagraphArray));
    if (ParArr == NULL)
    {
        exit(EXIT_FAILURE);
    }
    ParArr->ParArr = calloc(newsize, sizeof(struct Paragraph));
    int position2 = 0;
    int i;
    for (i = 0; f1(str + position2) >= 0; i++)
    {
        if (i + 1 == newsize)
        {
            newsize *= 2;
            ParArr->ParArr = realloc(ParArr->ParArr, newsize * sizeof(struct Paragraph));
            if (ParArr->ParArr == NULL)
            {
                exit(EXIT_FAILURE);
            }
        }
        ParArr->ParArr[i] = stringParagraph(str);
        ParArr->ParArr[i].index += position2;
        position2 += ParArr->ParArr[i].index + strlen(ParArr->ParArr[i].string);
    }
    ParArr->size = i * sizeof(struct Paragraph);
    return ParArr;
}

int fileExists(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  
    fclose(f);
    return 1;  
}

long getFileSize(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  
    fseek(f, 0, SEEK_END);  
    long fsize = ftell(f);  
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;  
}
