
#include <stdio.h>
#include <math.h>
#include <progbase/console.h>

int main(void)
{
    int x = 0;
    int y = 0;
    
    Console_clear();
    
    Console_setCursorAttribute(BG_INTENSITY_WHITE);

    for (y =1;y<=16;y++){
        for (x =1  ; x < 28; x++){
            Console_setCursorPosition(y, x);
            putchar(' ');
        }
    }
    Console_setCursorAttribute(BG_BLACK);
    for(x=18;x<=20;x++)
    {
    Console_setCursorPosition(1,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_BLACK);
    for(x=17;x<=18;x++){
    Console_setCursorPosition(2,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_BLACK);
    for(x=15;x<=16;x++){
    Console_setCursorPosition(3,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_BLACK);
    for(x=15;x<=15;x++){
    Console_setCursorPosition(4,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_BLACK);
    for(x=14;x<=15;x++){
    Console_setCursorPosition(5,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_BLACK);
    for(x=14;x<=14;x++){
    Console_setCursorPosition(6,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=7;x<=12;x++) { 
    Console_setCursorPosition(5,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=17;x<=22;x++){
    Console_setCursorPosition(5,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=5;x<=13;x++){
    Console_setCursorPosition(6,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=15;x<=25;x++){
    Console_setCursorPosition(6,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=3;x<=26;x++){
    Console_setCursorPosition(7,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=3;x<=26;x++){
    Console_setCursorPosition(8,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=2;x<=26;x++){
    Console_setCursorPosition(9,x);
    putchar(' ');
    }
    
    Console_setCursorAttribute(BG_RED);
    for(x=2;x<=26;x++){
    Console_setCursorPosition(10,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=2;x<=25;x++){
    Console_setCursorPosition(11,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=3;x<=25;x++){
    Console_setCursorPosition(12,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=3;x<=25;x++){
    Console_setCursorPosition(13,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=4;x<=24;x++){
    Console_setCursorPosition(14,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=6;x<=23;x++){
    Console_setCursorPosition(15,x);
    putchar(' ');
    }
  
    Console_setCursorAttribute(BG_YELLOW);
    for(x=19;x<=21;x++){
    Console_setCursorPosition(6,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_YELLOW);
    for(x=18;x<=24;x++){
    Console_setCursorPosition(7,x);
    putchar(' ');
    } 
    Console_setCursorAttribute(BG_YELLOW);
    for(x=19;x<=24;x++){
    Console_setCursorPosition(8,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_YELLOW);
    for(x=20;x<=24;x++){
    Console_setCursorPosition(9,x);
    putchar(' ');
    } 
    Console_setCursorAttribute(BG_YELLOW);
    for(x=20;x<=24;x++){
    Console_setCursorPosition(10,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_YELLOW);
    for(x=20;x<=23;x++){
    Console_setCursorPosition(11,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_YELLOW);
    for(x=20;x<=22;x++){
    Console_setCursorPosition(12,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_YELLOW);
    for(x=20;x<=21;x++){
    Console_setCursorPosition(13,x);
    putchar(' ');
    } 
    Console_setCursorAttribute(BG_BLACK);
    for(x=13;x<=15;x++){
    Console_setCursorPosition(15,x);
    putchar(' ');
    }    
    Console_setCursorAttribute(BG_RED);
    for(x=8;x<=11;x++){
    Console_setCursorPosition(16,x);
    putchar(' ');
    }
    Console_setCursorAttribute(BG_RED);
    for(x=17;x<=20;x++){
    Console_setCursorPosition(16,x);
    putchar(' ');
    }
    Console_reset();
    puts("");
    return 0;
}


