#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <string.h>
#include <assert.h>

int min2(int a, int b);
int min3(int a, int b, int c);
double f(double x, double y);
int doubleEquals(double a, double b);

int main(void)
{
    assert(min2(4, 6) == 4);
    assert(min2(0, 0) == 0);
    assert(min2(1, -4) == -4);

    assert(min3(4, 6, 8) == 4);
    assert(min3(0, 0, 9) == 0);
    assert(min3(1, -4, -5) == -5);
    assert(min3(1, -4, 100) == -4);

    assert(doubleEquals(f(1, 2), 5.0));
    assert(isnan(f(0, 2)));
    assert(doubleEquals(f(4.5, -3.4), 12.03140));
    return 0;
}

int doubleEquals(double a, double b)
{
    return fabs(a - b) < 0.00001;
}

int min2(int a, int b)
{
    if (a < b)
    {
        return a;
    }
    return b;
}

int min3(int a, int b, int c)
{
    return min2(min2(a, b), c);
}
double f(double x, double y)
{
    if (x == 0 || 1 / x < 0)
    {
        return NAN;
    }
    else
    {
        return sqrt(1 / x) + (pow(y, 2));
    }
}