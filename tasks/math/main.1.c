#include <stdio.h>

#define __LEN(A) sizeof((A)) / sizeof((A)[0])

struct Student {
    char name[100];
    int age;
    float score;
};

void printStudent(const struct Student * s) {
    printf("%s (%i years): %f\n", s->name, s->age, s->score);
    // s->age = 5;
}

void printStudents(int len, struct Student * students[len]) {
    int counter = 0;
    puts("============================");
    for (int i = 0; i < len; i++) {
        if (students[i] != NULL) {
            printStudent((const struct Student *)students[i]);
            counter++;
        }
    }
    puts("============================");
    printf("Total students: %i\n", counter);
}

void getArrMin(int len, int arr[len], int * minValue, int * minIndex);

int main(void) {
    
    struct Student atamaniuk = {
        "Olexiy Atamaniuk",
        17,
        1.9
    };
    
    struct Student * students[10];
    for (int i = 0; i < __LEN(students); i++) {
        students[i] = NULL;
    }
    
    printStudents(__LEN(students), students);
    students[0] = &atamaniuk;
    students[1] = &atamaniuk;
    students[2] = &atamaniuk;
    printStudents(__LEN(students), students);

    
    // int a[] = {0, -4, 5, 6, 7};
    // int minI = -1;
    // int minValue = 0;
    // getArrMin(__LEN(a), a, &minValue, &minI);
    
    // printf("min[%i]: %i\n", minI, minValue);
    
    
    return 0;
}


void getArrMin(int len, int arr[len], int * minValue, int * minIndex) {
    *minIndex = 0;
    int minVal = arr[0];
    for (int i = 1; i < len; i++) {
        if (arr[i] < minVal) {
            minVal = arr[i];
            *minIndex = i;
        }
    }
    *minValue = minVal;
}