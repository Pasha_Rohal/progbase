#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <progbase/console.h>
#include <progbase.h>
/*
Створити структуру даних, що описує положення і розміри прямокутника на двомірній площині. 
Заповнити поля структури значеннями. Вивести значення полів структури у консоль.
*/

struct Rect {
    int left; // x
    int top; // y
    int width; // 
    int height;
};

// test

void printRect(struct Rect r) {
    printf("[%i, %i, %i, %i]", r.left, r.top, r.width, r.height);
}

struct Rect createSquare(int x, int y, int size);

bool rectsIntersects(struct Rect a, struct Rect b);

struct ColorRect {
  struct Rect rect;
  int borderColor;
  int fillColor;
};
void drawRect(struct ColorRect rect);  // draw rect 
void drawRectAr(int len, struct ColorRect rect[len]);

int main() {
  //int color = BG_GREEN + INTENSITY;
  Console_clear();
    struct Rect r1;
    r1.left = 0;
    r1.top = 3;
    r1.width = 10;
    r1.height = 5;

    
    printRect(r1);
    
    struct Rect r2 = { 3, 4, 5, 5 };
    r2 = r1;
    printRect(r2);
    
    struct Rect s = createSquare(2, 2, 10);
    printRect(s);
    struct Rect r3 = {5,5,9,10};
    struct Rect r4 = {3,2,1,18};
    printRect(r3);
    printRect(r4);
    //printf("%i",rectsIntersects(r1,r2));
    printf("%i",rectsIntersects(r3,r4));

    
    // goo 111,
    return 0;
}

struct Rect createSquare(int x, int y, int size) {
    struct Rect s = { x, y, size, size};
    return s;
}


bool rectsIntersects(struct Rect a, struct Rect b)
{
    if(a.height>b.top+b.height||b.height>a.top+a.height && a.width>b.left+b.width||b.width>a.left+a.width && a.width>b.height)
    {
      return true;
    } 
    return false;  
}
void drawRect(struct ColorRect rect){
  int x;
  int y;
  Console_setCursorPosition(x,y);
  x=rect.rect.width;
  y=rect.rect.top;
  for(int i=y;i<=y+rect.rect.height;i++){
    for(int j=x;i<=x+rect.rect.left;i++){
      if(i==rect.rect.height||i==y && j==rect.rect.left||j==x){
        Console_setCursorAttribute(rect.borderColor);
      }else{
        Console_setCursorAttribute(rect.fillColor);
      }
      Console_setCursorAttribute(BG_DEFAULT);
    }
  }
}